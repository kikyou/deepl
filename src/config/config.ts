const repoList: repo[] = [
  {
    name: 'typescript',
    url: 'https://github.com/microsoft/TypeScript-Website.git',
    branch: 'v2',
    entry: ['packages/documentation/copy/en'],
    excludes: ['handbook-v1/'],
  },
  // {
  //   name: 'docker',
  //   url: 'https://github.com/docker/docker.github.io.git',
  //   includes: ['get-started', 'ci-cd'],
  // },
  {
    name: 'vscode',
    url: 'https://github.com/microsoft/vscode-docs.git',
    entry: ['docs/'],
    includes: ['setup/'],
    excludes: ['getstarted/introvideos.md'],
    branch: 'main',
  },
  // {
  //   name: 'gitlab',
  //   url: 'https://gitlab.com/gitlab-org/gitlab-docs',
  //   entry: ['docs'],
  //   branch: 'main',
  // },
];
export default repoList;
