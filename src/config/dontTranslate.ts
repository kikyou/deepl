export default {
  withSpace: ['Visual Studio Code', 'VS Code'],
  withOutSpace: ['Linux', 'Windows'],
  skip: [
    'C++',
    'C#',
    'Go',
    'Java',
    'Python',
    'ESLint',
    'JSHint',
    'PowerShell',
    'TypeScript',
    'Vue',
    'Webpack',
    'Yarn',
    'Chrome',
    'PHP XDebug',
    'Vim',
    'Sublime Text',
    'IntelliJ',
    'Emacs',
    'Atom',
    'Brackets',
    'Visual Studio',
    'Eclipse',
    'Yeoman',
    'npm',
    'macOS',
  ],
};
