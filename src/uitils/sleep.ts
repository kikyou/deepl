type sleep = (time: number) => Promise<void>;
const sleep: sleep = time =>
  new Promise(res => {
    setTimeout(() => {
      res();
    }, time);
  });
export default sleep;
