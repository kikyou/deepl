import type { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios';
import { shuffle } from 'lodash-es';
import { head } from 'ramda';
import spinner from '../uitils/ora';

interface IRespone<T> {
  data: T;
  code: number;
}

let mirrors = ['http://deepl.kikyou.ink', 'http://deepl.kikyou.ink:8080'];
mirrors = shuffle(mirrors);

class Api {
  instance: AxiosInstance;
  constructor(config: AxiosRequestConfig) {
    this.instance = axios.create(config);
    // 添加请求拦截器
    this.instance.interceptors.request.use(
      config => {
        config.baseURL = head(mirrors);
        if (mirrors.length > 1) {
          mirrors.push(mirrors.shift()!);
        }
        return config;
      },
      error => Promise.reject(error),
    );

    // 添加响应拦截器
    this.instance.interceptors.response.use(
      response => {
        if (response.data.code === 200) {
          return response;
        }
        spinner.text = `${response.config.baseURL} 已被禁止，换源中...`;
        if (mirrors.length > 1) {
          mirrors.pop();
          return this.instance(response.config);
        }
        return Promise.reject('已无可用服务');
      },
      error => {
        spinner.text = `${error.config.baseURL} 响应失败，换源中...`;
        if (mirrors.length > 1) {
          mirrors.pop();
          return this.instance(error.config);
        }
        return Promise.reject('服务异常');
      },
    );
  }
  request<T>(config: AxiosRequestConfig): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .request<T>(config)
        .then(data => {
          res(data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
  get<T, D = any>(url: string, config?: AxiosRequestConfig<D>): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .get<IRespone<T>, AxiosResponse<IRespone<T>, D>, D>(url, config)
        .then(data => {
          res(data.data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
  post<T, D = any>(url: string, data: D, config?: AxiosRequestConfig<D>): Promise<T> {
    return new Promise<T>((res, rej) => {
      this.instance
        .post<IRespone<T>, AxiosResponse<IRespone<T>, D>, D>(url, data)
        .then(data => {
          res(data.data.data);
        })
        .catch(err => {
          rej(err);
        });
    });
  }
}
const api = new Api({
  headers: {
    'Content-Type': 'application/json',
  },
});
export default api;
