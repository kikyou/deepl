import api from './index';

interface ITranslate {
  text: string;
  source_lang?: string;
  target_lang: string;
}
export default {
  // 登录
  translate(data: ITranslate) {
    return api.post<string, ITranslate>('/translate', data);
  },
};
