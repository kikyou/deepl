interface repo {
  /** **💡 仓库名称** */
  name: string;
  /** **💡 git 仓库地址** */
  url: string;
  /** **💡 分支** */
  branch?: string;
  /** **💡 文档存放的入口文件夹** */
  entry?: string[];
  /** **💡 包含的文件夹或者文件** */
  includes?: string[];
  /** **💡 排除的文件夹或者文件** */
  excludes?: string[];
}
