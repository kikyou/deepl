import { exec, execSync } from 'child_process';
import { compose, map, andThen, replace, bind } from 'ramda';
import { marked } from 'marked';
import { existsSync, readFileSync, writeFileSync } from 'node:fs';
import { join } from 'path';
import html2md from 'html-to-md';
import translate from './deepl';
import config from './src/config/config';
import sleep from './src/uitils/sleep';
import spinner from './src/uitils/ora';

// 清屏乐
console.clear();

const downloadSwitch = false; // 下载开关，开发用

// 同步配置中 item项 的 git 仓库
const downloadRepo = (repo: repo): Promise<void> =>
  new Promise((res, rej) => {
    let cwd;
    let command;
    const path = `./src/repo/${repo.name}`;
    if (existsSync(path)) {
      cwd = join(process.cwd(), path);
      command = 'git --version';
    } else {
      cwd = join(process.cwd(), './src/repo');
      command = `git clone  -b ${repo.branch || 'master'} ${repo.url} --depth=1 ${repo.name}`;
    }
    console.log(`同步远程仓库 ${repo.name} 中...`);
    exec(
      command,
      {
        cwd,
      },
      error => {
        if (error) {
          console.error(`同步仓库时出错: ${error}`);
          rej();
        }
        res();
      },
    );
  });

// 根据配置数组并行同步需要翻译的 git 仓库
if (downloadSwitch) {
  const downloadStart = compose(bind(Promise.all, Promise), map(downloadRepo));
  await downloadStart(config);
  console.log('所有仓库同步完毕');
}

/** **💡 读取文件** */
const readFile = (fileName: string): string => readFileSync(fileName, 'utf8');

/** **💡 修补微软引用符号以及专属页头，使其能正常解析词法** */
const fixMarkdownBug = compose(
  replace(/---((.|\n)+?)---/i, '```js$1```'),
  replace(/[^\n]\n>/gi, ''),
  replace(/_(\w+)_/gi, '$1'),
);

/** **💡 prettier** */
const prettier = () => {
  execSync('pnpm prettier', {
    cwd: process.cwd(),
  });
};

/** **💡 写入文件** */
const write = (data: string) => {
  writeFileSync('./output.md', data);
};
// ------------------------------ 分割线 ----------------------------------
const source = 'test.md';
/** **💡 主函数** */
const start = compose(
  andThen(compose(prettier, write, html2md)),
  translate,
  marked.parse,
  fixMarkdownBug,
  readFile,
);

console.log('Deepl v1.0 By SAI\n');
spinner.start('开始翻译...');
console.time('deepl');
await sleep(500);
await start(source);
spinner.succeed('已全部翻译完成');
console.timeEnd('deepl');
